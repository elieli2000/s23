//MAIN ACTIVITY:
/*
WDC028v1.5b-23 | JavaScript - Objects
Graded Activity:
Part 1: 
    1. Initialize/add the following trainer object properties:
      Name (String)
      Age (Number)
      Pokemon (Array)
      Friends (Object with Array values for properties)
    2. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
    3. Access the trainer object properties using dot and square bracket notation.
    4. Invoke/call the trainer talk object method.


Part 2:

1.) Create a new set of pokemon for pokemon battle. (Same as our discussion)
	- Solve the health of the pokemon that when tackle is invoked, current value of target's health should decrease continuously as many times the tackle is invoked.
	(target.health - this.attack)

2.) If health is less than or equal to 5, invoke faint function

*/

let myTrainer = {

	name: "Red",
	age: 22,
	pokemon: ["Charmander", "Bulbasor", "Pikachu"],
	friends: {
		Misty: "Gym Leader",
		Brock: "Geologist",
		Oak: "Pokemon Professor"
	} 
}



console.log(myTrainer)

let trainer = {
	talk: function() {
		console.log("Pikachu I choose you!");
	}
}


console.log(trainer)
trainer.talk()

function Pokemon(name, level) {

//properties
this.name = name
this.level = level
this.health = 3 * level
this.attack = 2 * level

//methods
this.tackle = function(target)
{
		
	console.log(this.name + " Tackled " + target.name)
	console.log(target.name + "'s health is now reduced " + (target.health - this.attack))
	target.health = target.health - this.attack;

if (target.health <= 5)
	console.log(target.name + " Fainted")
}




}

this.faint = function(target) 
{
 	

}

let Charmander = new Pokemon("Charmander", 50)
let Groudon = new Pokemon("Groudon", 200)



console.log(Charmander)
console.log(Groudon)

Charmander.tackle(Groudon)
Charmander.tackle(Groudon)
Charmander.tackle(Groudon)
Charmander.tackle(Groudon)
Charmander.tackle(Groudon)
Charmander.tackle(Groudon)




