/*
	Objects
		 an object is a data type that is used to represent real world objects. It is also a collection of related data and/or functionalities.

	Creating objects using object literal:
		Syntax:
			let objectName = {
					keyA: valueA,
					keyB: valueB
			}

*/

let student = {
	firstName: "Rupert",
	lastName: "Ramos",
	age: 30,
	StudentId: "2022-009752",
	email: ["rupert.ramos@mail.com", "RBR1209@gmail.com"],
	address: {
		street: "125 Ilang-Ilang St.",
		city: "Quezon City",
		country: "Philippines"
	}
}

console.log("Result from creating an object:")
console.log(student)
console.log(typeof student)


// Creating Objects using Constructor Function
/*
	Create a resuable function to create several objects that have the same data structure. This is useful for creating multiple instances/copies of an object.

	Syntax:
		function ObjectName(valueA, valueB) {
			this.keyA = valueA
			this.keyB = valueB
		}

		let variable = new function ObjectName(valueA, valueB)

		console.log(variable)

			-"this" is a keyword that is used for invoking; in refers to the global object
			- don't forget to add "new" keyword when creating the variables.
*/
// We use Pascal Casing for the ObjectName when creating objects using constructor function.
function Laptop(name, manufactureDate) {
	this.name = name 
	this.manufactureDate = manufactureDate
}

let laptop = new Laptop("Lenovo", 2008);
console.log("Result of creating objects using object constructor:")
console.log(laptop)

let myLaptop = new Laptop("MacBook Air", [2020, 2021])
console.log("Result of creating objects using object constructor:")
console.log(myLaptop)

let oldLaptop = Laptop("Portal R2E CCMC", 1980);
console.log("Result of creating objects using object constructor:")
console.log(oldLaptop)


// Creating empty object as placeholder
let computer = {}
let myComputer = new Object()
console.log(computer)
console.log(myComputer)

myComputer = {
	name: "Asus",
	manufactureDate: 2012
}

console.log(myComputer)

/*MINI ACTIVITY
	- Create an object constructor function to produce 2 objects with 3 key-value pairs
	- Log the 2 new objects in the console and send SS in our GC.

*/

function Camera(brand, pixels, manufactureDate) {
	this.brand = brand
	this.pixels = pixels
	this.manufactureDate = manufactureDate
}

let myCamera = new Camera("Nikon", "40MP", 2019)
let myFriendsCamera = new Camera("Canon", "20MP", 2008)
console.log("New Objects created using object constructor")
console.log(myCamera)
console.log(myFriendsCamera)

console.log("")
// Accessing Object Property

// Using the dot notation
// Syntax: objectName.propertyName
console.log("Result from don notation: " + myLaptop.name)

// Using the bracket notation
// Syntax: objectName["name"]
console.log("Result from don notation: " + myLaptop["name"])

// Accessing array objects

let array = [laptop, myLaptop];
// let array = [{name: "Lenovo", manufactureDate: 2008}, {name: MacBook Air, manufactureDate: [2019, 2020]}]

// Dot Notation
console.log(array[0].name)

// Square Bracker Notation
console.log(array[0]["name"])


// Initializing / Adding / Deleting / Reassigning Object Properties

let car = {}
console.log(car)

// Adding object properties
car.name = "Honda Civic"
console.log("Result from adding property using dot notation:")
console.log(car)

car["manufature date"] = 2019
console.log(car)

car.name = ["Ferrari", "Toyota"]
console.log(car)

 // Deleting objecting properties
delete car["manufature date"]
// car["manufature date"] = " "
console.log("Result from deleting object properties:")
console.log(car)

// Reassigning object properties
car.name = "Tesla"
console.log("Result from reassigning object property:")
console.log(car)

console.log(" ")

/* Object Method
	 a method where a function serves as a value in a property. They are also functions and one of the key differences they have is that methods are function related to a specific object property.


*/

let person = {
	name: "John",
	talk: function() {
		console.log("Hello! My name is " + this.name);
	}
}

console.log(person)
console.log("Result from object methods:")
person.talk()

person.walk = function() {
	console.log(this.name + " walked 25 steps forward.")
}

person.walk()


let friend = {
	firstName: "John",
	lastName: "Doe",
	address: {
		city: "Austin, Texas",
		country: "US"
	},
	emails: ["johnD@mail.com", "joe12@yahoo.com"],
	introduce: function() {
		console.log("Hello! My name is " + this.firstName + " " + this.lastName)
	}
}

friend.introduce()


/*Mini Activity*/


function mix(water, glass, ml)
{
	this.water = water
	this.glass = glass
	this.ml = ml
}

let firstGlass = new mix("Hot", 5, "100 ml");
console.log("firstGlass: ")
console.log(firstGlass)

let secondGlass = new mix("Cold", 10, "200 ml");
console.log("secondGlass: ")
console.log(secondGlass)

/*

Create Pokemon


*/


let myPokemon = {

	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function()
	{
		console.log("This pokemon Tackled Target Pokemon")

		console.log("Target Pokemon's Health is reduced to targetPokemonHealth")
	},

	Faint: function()
	{
		console.log("Pokemon Fainted")
	}
}

console.log(myPokemon)



function Pokemon(name, level) {

//properties
this.name = name
this.level = level
this.health = 3 * level
this.attack = 2 * level

//methods
this.tackle = function(target)
{
	console.log(this.name + " Tackled " + target.name)
	console.log(target.name + "'s health is now reduced " + (target.health - this.attack))

},

this.faint = function(target) 
{
 	this.health <= 0

	console.log(this.name + " Fainted")

}

}

let charizard = new Pokemon("Charizard", 12)
let squirtle = new Pokemon("Squirtle", 1)

console.log(charizard)
console.log(squirtle)

charizard.tackle(squirtle)

